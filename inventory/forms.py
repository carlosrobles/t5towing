# -*- coding: utf-8 -*-
# import our django dependencies
from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.db.models import Max
from django.forms import ModelChoiceField

# import our models and functions
from inventory.models import *
from inventory.utils import get_empty_lotspaces

# datepicker form to handle start and end dates for metrics
class DatePickerForm(forms.Form):
    start_date = forms.DateField()
    end_date = forms.DateField()

# employee form that handles displaying profile details. this is linked to the employee model.
class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'

# login form to handle authentication. we use widget attributes to further
# define corresponding form fields
class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username', 'placeholder': 'Username'}))
    password = forms.CharField(label="Password", max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password', 'placeholder': 'Password', 'type': 'password'}))

# lotspace model form that we use for vehicle check-in page. we define its __init__
# so that we can limit the contents of the 'number' drop down list to only
# empty lotspaces. this is linked to the lotspace model.
class LotSpaceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LotSpaceForm, self).__init__(*args, **kwargs)
        self.fields['number'] = forms.ChoiceField(get_empty_lotspaces(), required=False, widget=forms.Select())
    class Meta:
        model = LotSpace
        fields = '__all__'

# payment form that handles payments. this is linked to the payment model.
class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = '__all__'

# registration form that handles new user registration. this form is linked
# to the built-in django user account, which is in turn replicated across to
# the employee model. we use widget attributes to specify additional form
# parameters
class RegistrationForm(UserCreationForm):
    first_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'first_name', 'placeholder': 'Your First Name'}))
    last_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'last_name', 'placeholder': 'Your Last Name'}))
    address = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'address', 'placeholder': 'Your Physical Address'}))
    phone = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username', 'placeholder': '(555)555-5555'}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'address', 'phone', 'username', 'password1', 'password2')
        widgets = {
            'username' : forms.TextInput(attrs = {'class': 'form-control', 'name': 'username', 'placeholder': 'Username'}),
            'password1' : forms.PasswordInput(attrs = {'class': 'form-control', 'placeholder': 'Password'}),
            'password2' : forms.PasswordInput(attrs = {'class': 'form-control', 'placeholder': 'Required. Re-type your password'}),
        }

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs.update({'class': 'form-control'})
        self.fields['password2'].widget.attrs.update({'class': 'form-control'})

# simple lotspace form. this is linked to the lotspace model.
class SimpleLotSpaceForm(forms.ModelForm):
    class Meta:
        model = LotSpace
        fields = ('number',)

# towtrip form used to post and get towtrip details. this is linked to the
# towtrip model.
class TowTripForm(forms.ModelForm):
    class Meta:
        model = TowTrip
        fields = '__all__'

# uploadfile form used to handle file uploads
class UploadFileForm(forms.Form):
    file = forms.FileField()

# vehicle form that handles vehicle data. this is linked to the vehicle model.
class VehicleForm(forms.ModelForm):
    class Meta:
        model = Vehicle
        fields = '__all__'
