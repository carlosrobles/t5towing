# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-30 21:15
from __future__ import unicode_literals

from decimal import Decimal
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('address', models.CharField(max_length=100)),
                ('phone', models.CharField(max_length=100)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Fee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rate', models.DecimalField(decimal_places=2, max_digits=10)),
                ('timestamp', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Lot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField()),
                ('address', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='LotSpace',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=100)),
                ('full', models.BooleanField()),
                ('timestamp', models.DateTimeField()),
                ('lot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.Lot')),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstname', models.CharField(max_length=100)),
                ('lastname', models.CharField(max_length=100)),
                ('address', models.CharField(max_length=100)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=2)),
                ('zipcode', models.PositiveIntegerField()),
                ('phone', models.CharField(max_length=100)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=10)),
                ('card_type', models.CharField(blank=True, max_length=100, null=True)),
                ('card_cvv', models.PositiveIntegerField(blank=True, null=True)),
                ('card_number', models.CharField(blank=True, max_length=100, null=True)),
                ('expiration_month', models.CharField(blank=True, max_length=100, null=True)),
                ('expiration_year', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('timestamp', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='PaymentMetrics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('number_of_payments', models.PositiveIntegerField()),
                ('sum_of_payments', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
        ),
        migrations.CreateModel(
            name='TowTrip',
            fields=[
                ('timestamp', models.DateTimeField(primary_key=True, serialize=False)),
                ('pickupaddress', models.CharField(max_length=100)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='TowTripMetrics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('number_of_tows', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='TowTruck',
            fields=[
                ('vin', models.CharField(max_length=100, primary_key=True, serialize=False)),
                ('make', models.CharField(max_length=100)),
                ('model', models.CharField(max_length=100)),
                ('year', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('color', models.CharField(max_length=100)),
                ('license_num', models.CharField(max_length=100)),
                ('license_state', models.CharField(max_length=2)),
                ('description', models.CharField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('vin', models.CharField(max_length=100, primary_key=True, serialize=False)),
                ('make', models.CharField(max_length=100)),
                ('model', models.CharField(max_length=100)),
                ('year', models.PositiveSmallIntegerField()),
                ('color', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=300)),
                ('damage', models.CharField(blank=True, max_length=500, null=True)),
                ('license_num', models.CharField(max_length=100)),
                ('license_state', models.CharField(max_length=2)),
                ('balance', models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=10)),
                ('checkedin', models.BooleanField()),
            ],
        ),
        migrations.AddField(
            model_name='towtrip',
            name='towtruck',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.TowTruck'),
        ),
        migrations.AddField(
            model_name='towtrip',
            name='vehicle',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.Vehicle'),
        ),
        migrations.AddField(
            model_name='payment',
            name='vehicle',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.Vehicle'),
        ),
        migrations.AddField(
            model_name='lotspace',
            name='towtrip',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='inventory.TowTrip'),
        ),
    ]
