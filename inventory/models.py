# -*- coding: utf-8 -*-
# import our python dependencies
from __future__ import unicode_literals
from decimal import Decimal

# import our django dependencies
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible

# employee model that is tied to the django user authentication system
# every registered user automatically gets an employee record
@python_2_unicode_compatible
class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)

# fee model that is used to calculate vehicle balance
@python_2_unicode_compatible
class Fee(models.Model):
    rate = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp = models.DateTimeField()
    def __str__(self):
        return str(self.rate)
    def __unicode__(self):
        return unicode(str(self.rate))

# lot model that contains lotspaces
@python_2_unicode_compatible
class Lot(models.Model):
    number = models.IntegerField()
    address = models.CharField(max_length=100)
    def __str__(self):
        return self.address
    def __unicode__(self):
        return unicode(self.address)

# lotspace model that keeps track of whether it is full and what towtrip
# it is associated with
@python_2_unicode_compatible
class LotSpace(models.Model):
    number = models.CharField(max_length=100)
    lot = models.ForeignKey(Lot, on_delete=models.CASCADE)
    full = models.BooleanField()
    towtrip = models.ForeignKey('TowTrip', on_delete=models.CASCADE, blank=True, null=True)
    timestamp = models.DateTimeField()
    def __str__(self):
        return self.number

# payment model that will keep track of customer information as well as
# what vehicle the payment was applied to
@python_2_unicode_compatible
class Payment(models.Model):
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=2)
    zipcode = models.PositiveIntegerField()
    phone = models.CharField(max_length=100)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    vehicle = models.ForeignKey('Vehicle', on_delete=models.CASCADE)
    card_type = models.CharField(max_length=100,blank=True, null=True)
    card_cvv = models.PositiveIntegerField(blank=True, null=True)
    card_number = models.CharField(max_length=100,blank=True, null=True)
    expiration_month = models.CharField(max_length=100,blank=True, null=True)
    expiration_year = models.PositiveSmallIntegerField(blank=True, null=True)
    timestamp = models.DateTimeField()
    def __str__(self):
        return "%s%s %s %s %s %s" % ('$', self.amount, '@', self.timestamp, 'for VIN', self.vehicle)

# payment metrics model that keeps a daily tally of submitted payments
@python_2_unicode_compatible
class PaymentMetrics(models.Model):
    date = models.DateField()
    number_of_payments = models.PositiveIntegerField()
    sum_of_payments = models.DecimalField(max_digits=10, decimal_places=2)
    def __str__(self):
        return "%s %s %s %s %s" % (self.number_of_payments, 'payments for a total of', self.sum_of_payments, 'on', self.date)

# towtrip model that will keep track of towing details for every vehicle
@python_2_unicode_compatible
class TowTrip(models.Model):
    timestamp = models.DateTimeField(primary_key=True)
    pickupaddress = models.CharField(max_length=100)
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE)
    towtruck = models.ForeignKey('TowTruck', on_delete=models.CASCADE)
    vehicle = models.ForeignKey('Vehicle', on_delete=models.CASCADE)
    def __str__(self):
        return "%s %s %s %s %s" % (self.vehicle, '@', self.timestamp, 'from', self.pickupaddress)

# towtripmetrics model that keeps a daily tally of tow trips for metrics purposes
@python_2_unicode_compatible
class TowTripMetrics(models.Model):
    date = models.DateField()
    number_of_tows = models.PositiveIntegerField()
    def __str__(self):
        return "%s %s %s" % (self.number_of_tows, 'tows on', self.date)

# towtruck model that contains tow truck details
@python_2_unicode_compatible
class TowTruck(models.Model):
    vin = models.CharField(max_length=100, primary_key=True)
    make = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    year = models.PositiveSmallIntegerField(blank=True, null=True)
    color = models.CharField(max_length=100)
    license_num = models.CharField(max_length=100)
    license_state = models.CharField(max_length=2)
    description = models.CharField(max_length=300)
    def __str__(self):
        return "%s %s %s %s" % (self.year, self.make, self.model, self.description)

# vehicle model that contains vehicle details
@python_2_unicode_compatible
class Vehicle(models.Model):
    vin = models.CharField(max_length=100, primary_key=True)
    make = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    year = models.PositiveSmallIntegerField()
    color = models.CharField(max_length=100)
    description = models.CharField(max_length=300)
    damage = models.CharField(max_length=500, null=True, blank=True)
    license_num = models.CharField(max_length=100)
    license_state = models.CharField(max_length=2)
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    checkedin = models.BooleanField()
    def __str__(self):
        return "%s %s %s %s" % (self.year, self.make, self.model, self.description)

# implement receiver that watches for creation of user objects
# If received, a corresponding employee object is also created
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Employee.objects.create(user=instance)

# implement receiver that watches for update of user objects
# If received, the corresponding employee object is also updated
@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.employee.save()
