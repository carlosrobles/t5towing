# -*- coding: utf-8 -*-
# import our django dependencies
from django.conf.urls import url
from django_filters.views import FilterView

# import our views and filters
from inventory import views

# configure our app name
app_name = 'inventory'

# set our url patterns
urlpatterns = [
    # /inventory/
    url(r'^$', views.index, name='inventory'),

    # /inventory/checkin/
    url(r'^checkin/$', views.checkin, name='checkin'),

    # /inventory/import/
    url(r'^import/$', views.file_upload, name='file_upload'),

    # /inventory/badupload/
    url(r'^badupload/$', views.badupload, name='badupload'),

    # /inventory/search/
    url(r'^search/$', views.search, name='search'),

    # /inventory/lotspace
    url(r'^lotspace/$', views.lotspace, name='lotspace'),

    # /inventory/detail/<vin>/
    url(r'^detail/(?P<vin>[\w-]+)$', views.detail, name='detail'),

    # /inventory/payment/
    url(r'^payment/$', views.payment, name='payment'),

    # /inventory/payment/<vin>/
    url(r'^payment/(?P<vin>[\w-]+)$', views.payment, name='payment'),
]
