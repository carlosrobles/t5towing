# -*- coding: utf-8 -*-
# import our python dependencies
import csv, os
import datetime
from decimal import Decimal

# import our django dependencies
from django.contrib.auth.models import User
from django.contrib import messages
from django.db.models import Sum
from django.utils import timezone

# import our models
from inventory.models import Employee, Fee, Lot, LotSpace, Payment, PaymentMetrics, TowTrip, TowTripMetrics, TowTruck, Vehicle

# this function calculates vehicle balance by multiplying the rate with
# number of days in the lot, then subtracting any payments applied to the
# vehicle
# we assume that this function is being called because the vehicle
# is occupying a lot space.
def calculate_vehicle_balance(vin):
    # get vehicle
    vehicle = Vehicle.objects.get(vin=vin)

    # calculate vehicle balance if vehicle is checked in to lot. Otherwise,
    # balance is $0.00
    if vehicle.checkedin == True:
        # get fare
        fare = Fee.objects.latest('timestamp')
        rate = fare.rate

        # get last towtrip for vehicle
        towtrip = TowTrip.objects.filter(vehicle=vin).latest('timestamp')

        # get lotspace for towtrip
        lotspace = LotSpace.objects.get(towtrip=towtrip.timestamp)

        # get payments for vehicle after last towtrip and their sum
        payments = Payment.objects.filter(vehicle=vin, timestamp__gte=towtrip.timestamp)
        payments_sum = Payment.objects.filter(vehicle=vin, timestamp__gte=towtrip.timestamp).aggregate(Sum('amount'))
        for key,value in payments_sum.iteritems():
            credits = value

        # calculate vehicle balance
        towtrip_time = towtrip.timestamp
        now = timezone.now()
        delta = now - towtrip_time
        charges = rate * (delta.days + 1)

        if credits:
            charges = charges - credits

        vehicle.balance = charges
        vehicle.save()
        return(vehicle.balance)
    else:
        # set balance to $0.00 and return
        vehicle.balance = '0.00'
        return(vehicle.balance)

# this function goes through all lotspace numbers and gets the latest record
# for each. It then only returns a list of empty lotspaces represented by key-value pairs
def get_empty_lotspaces():
    lotspace_sections = ['A','B','C','D']
    lotspace_list = []
    empty_lotspaces = []
    empty_lotspaces.append(('','None'))

    for section in lotspace_sections:
        for x in range(1,251):
            str_x = str(x).zfill(3)
            lotspace_number = (section + str_x)
            lotspace_list.append(LotSpace.objects.filter(number=lotspace_number).latest('timestamp'))

    for lotspace in lotspace_list:
        if lotspace.full is not True:
            empty_lotspaces.append((lotspace.number, lotspace.number))

    return sorted(empty_lotspaces)

# this function reads the filename of the uploaded file and runs the appropriate function
def import_csv_to_model(f):
    filename, filenameExtension = os.path.splitext(str(f))
    switcher = {
        'fee': import_csv_to_Fee,
        'lot': import_csv_to_Lot,
        'lotspace': import_csv_to_LotSpace,
        'payment': import_csv_to_Payment,
        'towtrip': import_csv_to_TowTrip,
        'towtruck': import_csv_to_TowTruck,
        'vehicle': import_csv_to_Vehicle,
    }

    function = switcher.get(filename)
    return function(f)

# this function imports fee records based on fee.csv
def import_csv_to_Fee(f):
    csvlist = []
    csvReader = csv.DictReader(f)
    for row in csvReader:
        obj, created = Fee.objects.get_or_create(
            rate = row['rate'],
            timestamp = row['timestamp']
        )
        csvlist.append(row)
    return (csvlist)

# this function imports lot records based on lot.csv
def import_csv_to_Lot(f):
    csvlist = []
    csvReader = csv.DictReader(f)
    for row in csvReader:
        obj, created = Lot.objects.get_or_create(
            number = row['number'],
            address = row['address']
        )
        csvlist.append(row)
    return (csvlist)

# this function imports lotspace records based on lotspace.csv
# for our foreign keys, we must first instantiate the related model instances
# before we can assign them as foreign keys
def import_csv_to_LotSpace(f):
    csvlist = []
    csvReader = csv.DictReader(f)
    for row in csvReader:
        obj_lot = Lot.objects.get(number=row['lot'])
        if row['towtrip'] != '':
            obj_towtrip = TowTrip.objects.get(timestamp=row['towtrip'])
            obj, created = LotSpace.objects.get_or_create(
                number = row['number'],
                lot = obj_lot,
                full = row['full'],
                towtrip = obj_towtrip,
                timestamp = row['timestamp']
            )
        else:
            obj, created = LotSpace.objects.get_or_create(
                number = row['number'],
                lot = obj_lot,
                full = row['full'],
                timestamp = row['timestamp']
            )
        csvlist.append(row)
    return (csvlist)

# this function imports payment records based on payment.csv
# for our foreign keys, we must first instantiate the related model instances
# before we can assign them as foreign keys
def import_csv_to_Payment(f):
    csvlist = []
    csvReader = csv.DictReader(f)
    for row in csvReader:
        obj_vehicle = Vehicle.objects.get(vin=row['vehicle'])
        obj, created = Payment.objects.get_or_create(
            firstname = row['firstname'],
            lastname = row['lastname'],
            address = row['address'],
            city = row['city'],
            state = row['state'],
            zipcode = row['zipcode'],
            phone = row['phone'],
            amount = row['amount'],
            vehicle = obj_vehicle,
            timestamp = row['timestamp']
        )
        csvlist.append(row)

        # every time a payment is added, we take note of the date
        # and add a tally to the number and sum oof payments for
        # that date
        if created == True:
            timestamp = datetime.datetime.strptime(row['timestamp'],"%Y-%m-%d %H:%M")
            try:
                payment_metrics = PaymentMetrics.objects.get(date=timestamp.date())
            except PaymentMetrics.DoesNotExist:
                payment_metrics = PaymentMetrics.objects.create(date=timestamp.date(), number_of_payments=0, sum_of_payments=0.00)

            payment_metrics.number_of_payments += 1
            old_sum = Decimal(payment_metrics.sum_of_payments)
            new_sum = old_sum + Decimal(row['amount'])
            payment_metrics.sum_of_payments = new_sum
            payment_metrics.save()

    return (csvlist)

# this function imports towtrip records based on towtrip.csv
# for our foreign keys, we must first instantiate the related model instances
# before we can assign them as foreign keys
def import_csv_to_TowTrip(f):
    csvlist = []
    csvReader = csv.DictReader(f)

    for row in csvReader:
        employees = Employee.objects.all()
        for employee in employees:
            if employee.user.username == row['employee']:
                obj_employee = employee
        obj_user = User.objects.get(username=row['employee'])
        obj_employee = obj_user.employee
        obj_towtruck = TowTruck.objects.get(vin=row['towtruck'])
        obj_vehicle = Vehicle.objects.get(vin=row['vehicle'])

        obj, created = TowTrip.objects.get_or_create(
            timestamp = row['timestamp'],
            pickupaddress = row['pickupaddress'],
            employee = obj_employee,
            towtruck = obj_towtruck,
            vehicle = obj_vehicle
        )
        csvlist.append(row)

        # every time a towtrip is added, we take note of the date
        # and add a tally to the number of tows for that date
        if created == True:
            timestamp = datetime.datetime.strptime(row['timestamp'],"%Y-%m-%d %H:%M")
            # Keep track of when each tow takes place for metrics purposes
            try:
                towtrip_metrics = TowTripMetrics.objects.get(date=timestamp.date())
            except TowTripMetrics.DoesNotExist:
                towtrip_metrics = TowTripMetrics.objects.create(date=timestamp.date(), number_of_tows=0)

            towtrip_metrics.number_of_tows += 1
            towtrip_metrics.save()

    return (csvlist)

# this function imports towtruck records based on towtruck.csv
def import_csv_to_TowTruck(f):
    csvlist = []
    csvReader = csv.DictReader(f)
    for row in csvReader:
        obj, created = TowTruck.objects.get_or_create(
            vin = row['vin'],
            make = row['make'],
            model = row['model'],
            year = row['year'],
            color = row['color'],
            description = row['description'],
            license_num = row['license_num'],
            license_state = row['license_state']
        )
        csvlist.append(row)
    return (csvlist)

# this function imports vehicle records based on vehicle.csv
def import_csv_to_Vehicle(f):
    csvlist = []
    csvReader = csv.DictReader(f)
    for row in csvReader:
        obj, created = Vehicle.objects.get_or_create(
            vin = row['vin'],
            make = row['make'],
            model = row['model'],
            year = row['year'],
            color = row['color'],
            description = row['description'],
            damage = row['damage'],
            license_num = row['license_num'],
            license_state = row['license_state'],
            checkedin = True
        )
        csvlist.append(row)
    return (csvlist)

# this function takes the input vehicle and checks it out after confirming
# that the balance is $0.00
def vehicle_check_out(request, vin):
    # instantiate objects for the vehicle and its associated towtrip and lotspace
    vehicle = Vehicle.objects.get(vin=vin)
    towtrip = vehicle.towtrip_set.latest('timestamp')
    lotspace = towtrip.lotspace_set.latest('timestamp')

    # if the vehicle is already checked out, we display an error, otherwise
    # we proceed with checkout after confirming that the balance is $0.00
    if vehicle.checkedin == True:
        if vehicle.balance != 0:
            messages.error(request, "The vehicle balance must be $0.00 for a successful checkout")
            return False
        else:
            vehicle.checkedin = False
            new_lotspace = LotSpace.objects.create(number=lotspace.number, lot=lotspace.lot, full=False, timestamp=timezone.now())
            vehicle.save()
            new_lotspace.save()
            messages.success(request, "Vehicle" + " '" + vin + "' " + "has been successfully checked out")
            return True
    else:
        messages.error(request, "This vehicle is already checked out.")
