# -*- coding: utf-8 -*-
# import our django dependencies
import django_filters

# import our models
from inventory.models import *

# lotspace filter we use on the lotspace dashboard page
class LotSpaceFilter(django_filters.FilterSet):
    class Meta:
        model = LotSpace
        fields = ['number', 'lot', 'full', 'towtrip', 'timestamp']

# payment filter that we use on the payment summary page. we specify case-insensitive
# 'contains' matches
class PaymentFilter(django_filters.FilterSet):
    vehicle__vin = django_filters.CharFilter(lookup_expr='icontains')
    class Meta:
        model = Payment
        fields = ['firstname', 'lastname', 'zipcode', 'vehicle__vin', 'amount']

# vehicle filter that we use on the search page. we specify case-insensitive
# exact matches
class VehicleFilter(django_filters.FilterSet):
    vin = django_filters.CharFilter(lookup_expr='iexact')
    license_num = django_filters.CharFilter(lookup_expr='iexact')
    license_state = django_filters.CharFilter(lookup_expr='iexact')
    class Meta:
        model = Vehicle
        fields = ['vin', 'make', 'model', 'color', 'description', 'license_num', 'license_state', 'balance', 'checkedin']
