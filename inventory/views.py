# -*- coding: utf-8 -*-
# import our python dependencies
from __future__ import unicode_literals
import datetime
import re
from decimal import Decimal

# import our django dependencies
from django import forms
from django.contrib import messages
from django.contrib.auth import login as auth_login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Max
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone

# import the graphos graphing library
from graphos.sources.model import ModelDataSource
from graphos.renderers.gchart import ColumnChart

# import all of our custom filters, forms, functions, and models
from inventory.filters import LotSpaceFilter, PaymentFilter, VehicleFilter
from inventory.forms import DatePickerForm, EmployeeForm, LoginForm, LotSpaceForm, PaymentForm, PaymentMetrics, RegistrationForm, TowTripForm, TowTripMetrics, VehicleForm, SimpleLotSpaceForm, UploadFileForm
from inventory.models import LotSpace, Payment, TowTrip, Vehicle
from inventory.utils import import_csv_to_model, calculate_vehicle_balance, vehicle_check_out

# bad file upload error page
# this view displays an error page if there is a problem uploading a file
@login_required(login_url="/login")
def badupload(request):
    return HttpResponse('There was a problem with your upload')

# vehicle check-in
# this view takes all of the input fields, copies/assigns them to their
# appropriate form, and then saves the associated objects to the database
@login_required(login_url="/login")
def checkin(request):
    if request.POST:
        # because request.POST is immutable, we make a copy of it and modify
        # it as needed before we copy the modified contents back into the
        # request.POST. In this case, we are copying around form entries
        # in order to complete the necessary form entries for all three forms:
        # vehicle, towtrip, and lotspace
        post_copy = request.POST.copy()
        post_copy['towtrip-vehicle'] = post_copy['vehicle-vin']
        post_copy['lotspace-towtrip'] = post_copy['towtrip-timestamp']
        post_copy['vehicle-checkedin'] = True
        post_copy['lotspace-full'] = True
        post_copy['lotspace-timestamp'] = timezone.now()
        post_copy['vehicle-balance'] = '0.00'
        request.POST = post_copy

        # assign vehicle to this check-in if it already exists, otherwise
        # create a new entry
        try:
            vehicle = Vehicle.objects.get(vin=request.POST['vehicle-vin'])
            vehicle_form = VehicleForm(instance=vehicle)
        except Vehicle.DoesNotExist:
            vehicle_form = VehicleForm(request.POST, prefix="vehicle")

        # save vehicle_form and then refresh from database if form is valid
        if vehicle_form.is_valid():
            vehicle = vehicle_form.save()
            vehicle.refresh_from_db()
            towtrip_form = TowTripForm(request.POST, prefix="towtrip")

            # save towtrip_form and then refresh from database if form is valid
            if towtrip_form.is_valid():
                towtrip = towtrip_form.save()
                towtrip.refresh_from_db()

                # every time a towtrip is added, we take note of the date
                # and add a tally to the number of tows for that date
                timestamp = towtrip_form.cleaned_data['timestamp']
                try:
                    towtrip_metrics = TowTripMetrics.objects.get(date=timestamp.date())
                except TowTripMetrics.DoesNotExist:
                    towtrip_metrics = TowTripMetrics.objects.create(date=timestamp.date(), number_of_tows=0)
                towtrip_metrics.number_of_tows += 1
                towtrip_metrics.save()
                # save lotspace_form and then refresh from database if form is valid
                lotspace_form = LotSpaceForm(request.POST, prefix="lotspace")

                if lotspace_form.is_valid():
                    lotspace = lotspace_form.save()
                    lotspace.refresh_from_db()
                    messages.success(request, 'Your vehicle has been successfully checked in to the system!')
                else:
                    messages.error(request, 'Your lot space form is invalid!')
            else:
                messages.error(request, 'Your tow trip form is invalid!')
        else:
            messages.error(request, 'Your vehicle form is invalid!')

        return redirect('inventory:checkin')
    else:
        vehicle_form = VehicleForm(prefix='vehicle')
        towtrip_form = TowTripForm(prefix='towtrip')
        lotspace_form = LotSpaceForm(prefix='lotspace')

    return render(request, 'inventory/checkin.html', {'vehicle_form': vehicle_form, 'towtrip_form': towtrip_form, 'lotspace_form': lotspace_form})

# vehicle detail
# for every vehicle, this detail view displays vehicle, latest towtrip, and
# lotspace details
def detail(request, vin):
    calculate_vehicle_balance(vin)
    vehicle = Vehicle.objects.get(vin=vin)
    towtrip = TowTrip.objects.filter(vehicle=vin).latest('timestamp')
    vehicle_form = VehicleForm(instance=vehicle)
    towtrip_form = TowTripForm(instance=towtrip)

    if vehicle.checkedin == True:
        lotspace = LotSpace.objects.get(towtrip=towtrip.timestamp)
        lotspace_form = SimpleLotSpaceForm(instance=lotspace)
    else:
        lotspace_form = SimpleLotSpaceForm()

    # if the logged-in user submits a checkout request, we run the checkout
    # function
    if request.POST:
        checkout = vehicle_check_out(request, vin)
        return redirect('inventory:detail', vin=vin)

    return render(request, 'inventory/detail.html', {'vin': vin, 'vehicle_form': vehicle_form, 'towtrip_form': towtrip_form, 'lotspace_form': lotspace_form})

# upload csv files
# this view takes an input file and assigns it to the UploadFileForm form.
# if valid, the UploadFileForm is processed by the utils.import_csv_to_model
# function in order to add the csv data to the database
@login_required(login_url="/login")
def file_upload(request):
    if request.POST and request.FILES:
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            rawcsv = import_csv_to_model(request.FILES['file'])
            messages.success(request, 'Your file was uploaded successfully!')
            return redirect('inventory:file_upload')
        else:
            return redirect('inventory:badupload')
    else:
        form = UploadFileForm()
    return render(request, 'inventory/import.html', {'form': form})

# login
# this view processes login requests
def login(request):
    if request.POST:
        form = LoginForm(request.POST)
        return render(request, 't5towing/login.html', {'form': form})
    else:
        form = LoginForm()
        return render(request, 't5towing/login.html', {'form': form})

# root homepage
def index(request):
    return redirect('home')

# lotspace capacity
# this view goes through all the lotspace data to find the latest state of
# of every lotspace. It then populates a set of meters that indicate how full
# the lot is
@login_required(login_url="/login")
def lotspace(request):
    # initialize our variables
    lotspace_sections = ['A','B','C','D']
    perm_lotspace_list = []
    temp_lotspace_list = []
    full_a_count = 0
    full_b_count = 0
    full_c_count = 0
    full_d_count = 0

    # Collect the latest record for every lot space
    for section in lotspace_sections:
        temp_lotspace_list = []
        for x in range(1,251):
            str_x = str(x).zfill(3)
            temp_lotspace_list.append(LotSpace.objects.filter(number=section + str_x).latest('towtrip'))
            perm_lotspace_list.append(LotSpace.objects.filter(number=section + str_x).latest('towtrip'))

        # keep tally when lotspaces are full
        for space in temp_lotspace_list:
            if space.full == True:
                if section == 'A':
                    full_a_count += 1
                if section == 'B':
                    full_b_count += 1
                if section == 'C':
                    full_c_count += 1
                if section == 'D':
                    full_d_count += 1

    # generate the variables we will pass to the html page
    full_a_count = float(full_a_count)
    total_a_count = float(len(temp_lotspace_list))
    fraction_a_full = float(full_a_count/total_a_count)
    percent_a_full = int(fraction_a_full*100)

    full_b_count = float(full_b_count)
    total_b_count = float(len(temp_lotspace_list))
    fraction_b_full = float(full_b_count/total_b_count)
    percent_b_full = int(fraction_b_full*100)

    full_c_count = float(full_c_count)
    total_c_count = float(len(temp_lotspace_list))
    fraction_c_full = float(full_c_count/total_c_count)
    percent_c_full = int(fraction_c_full*100)

    full_d_count = float(full_d_count)
    total_d_count = float(len(temp_lotspace_list))
    fraction_d_full = float(full_d_count/total_d_count)
    percent_d_full = int(fraction_d_full*100)

    # generate the filter data that will populate the lotspace table
    #all_lotspaces = LotSpace.objects.all()
    #all_lotspaces = LotSpace.objects.values('number', 'full', 'timestamp').annotate(latest_record=Max('timestamp')).order_by('number')
    #lotspace_filter = LotSpaceFilter(request.GET, queryset=all_lotspaces)
    return render(request, 'inventory/lotspace.html', {'lotspace_list': perm_lotspace_list, 'lotspace_a_percent': percent_a_full, 'lotspace_b_percent': percent_b_full, 'lotspace_c_percent': percent_c_full, 'lotspace_d_percent': percent_d_full})

# metrics
# this view displays a graph of towtrip frequency for the dates chosen from
# the datepicker. By default, it shows all of the data.
@login_required(login_url="/login")
def metrics(request):
    # if the user inputs a start and end date, the graph will show only
    # data points within the date range. Otherwise, all data points will be
    # shown
    if request.POST:
        datepicker_form = DatePickerForm(request.POST)
        if datepicker_form.is_valid():
            start_date = datepicker_form.cleaned_data['start_date']
            end_date = datepicker_form.cleaned_data['end_date']
            towtrip_queryset = TowTripMetrics.objects.filter(date__range=(start_date, end_date)).order_by('date')
            payment_queryset = PaymentMetrics.objects.filter(date__range=(start_date, end_date)).order_by('date')
        else:
            towtrip_queryset = TowTripMetrics.objects.order_by('date')
            payment_queryset = PaymentMetrics.objects.order_by('date')
            messages.error(request, "Your datepicker form is invalid!")
    else:
        towtrip_queryset = TowTripMetrics.objects.order_by('date')
        payment_queryset = PaymentMetrics.objects.order_by('date')

    # we create our data source based on the towtrip_queryset defined above
    towtrip_data_source = ModelDataSource(towtrip_queryset, fields=['date', 'number_of_tows'])
    towtrip_chart = ColumnChart(towtrip_data_source, height=500, width=1100)

    payment_number_data_source = ModelDataSource(payment_queryset, fields=['date', 'number_of_payments'])
    payment_number_chart = ColumnChart(payment_number_data_source, height=500, width=1100)

    payment_sum_data_source = ModelDataSource(payment_queryset, fields=['date', 'sum_of_payments'])
    payment_sum_chart = ColumnChart(payment_sum_data_source, height=500, width=1100, options={'vAxis':{'format':'currency'}})
    return render(request, 't5towing/metrics.html', {'towtrip_chart': towtrip_chart, 'payment_number_chart': payment_number_chart, 'payment_sum_chart': payment_sum_chart})

# payment
# this view records user payment details sans any credit card information
# and applies the payment toward the balance of the corresponding input vin
def payment(request, vin='None'):
    if request.POST:
        # because request.POST is immutable, we make a copy of it and modify
        # it as needed before we copy the modified contents back into the
        # request.POST. In this case, we are populating the timetamp field
        post_copy = request.POST.copy()
        post_copy['timestamp'] = timezone.now()
        request.POST = post_copy

        # throw an error if we can't find a matching vehicle in the database
        # otherwise, process the form with is_valid()
        try:
            vehicle = Vehicle.objects.get(vin=request.POST['vehicle'])
        except Vehicle.DoesNotExist:
            vehicle = 'None'
        if vehicle == 'None':
            messages.error(request, "There is no vehicle with VIN '" + request.POST['vehicle'] + "'")
            return redirect('inventory:payment')
        else:
            form = PaymentForm(request.POST)
            # ensure payment amount is not greater than balance on vehicle
            # process payments by subtracting amount from vehicle balance
            if form.is_valid():
                calculate_vehicle_balance(request.POST['vehicle'])
                vehicle = Vehicle.objects.get(vin=request.POST['vehicle'])
                if form.cleaned_data['amount'] > vehicle.balance:
                    messages.error(request, 'The payment amount is greater than the balance owed on the vehicle.')
                    return redirect('inventory:payment')
                else:
                    new_payment = form.save()
                    vehicle.balance = vehicle.balance - form.cleaned_data['amount']
                    vehicle.save()

                    # every time a payment is added, we take note of the date
                    # and add a tally to the number and sum of payments for
                    # that date
                    timestamp = timezone.now()
                    try:
                        payment_metrics = PaymentMetrics.objects.get(date=timestamp.date())
                    except PaymentMetrics.DoesNotExist:
                        payment_metrics = PaymentMetrics.objects.create(date=timestamp.date(), number_of_payments=0, sum_of_payments=0.00)

                    payment_metrics.number_of_payments += 1
                    old_sum = Decimal(payment_metrics.sum_of_payments)
                    new_sum = old_sum + form.cleaned_data['amount']
                    payment_metrics.sum_of_payments = new_sum
                    payment_metrics.save()

                    messages.success(request, 'Your payment has been successfully submitted!')
                    return redirect('inventory:payment')
            else:
                messages.error(request, 'There was an error with your form.')
                return redirect('inventory:payment')
    else:
        try:
            vehicle = Vehicle.objects.get(vin=vin)
            balance = calculate_vehicle_balance(vin)
        except Vehicle.DoesNotExist:
            vehicle = Vehicle.objects.none()

        form = PaymentForm()
    return render(request, 'inventory/payment.html', {'form': form, 'vin': vin, 'vehicle': vehicle})

# payment summary
# this view uses a filter to search through payment records based on vin or license
# number and state
@login_required(login_url="/login")
def payment_summary(request):
    payment_list = Payment.objects.all()
    payment_filter = PaymentFilter(request.GET, queryset=payment_list)
    return render(request, 't5towing/payment_summary.html', {'filter': payment_filter })

# profile detail
# for every employee, this view displays detailed info
@login_required(login_url="/login")
def profile(request):
    obj_user = request.user
    obj_employee = obj_user.employee
    employee_form = EmployeeForm(instance=obj_employee)
    return render(request, 't5towing/profile.html', {'employee': obj_user.username, 'employee_form': employee_form})

# registration
# this view processes registration forms and creates new employee accounts
# that can log in to the site
def registration(request):
    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.employee.first_name = form.cleaned_data.get('first_name')
            user.employee.last_name = form.cleaned_data.get('last_name')
            user.employee.address = form.cleaned_data.get('address')
            user.employee.phone = form.cleaned_data.get('phone')
            user.save()
            password = form.cleaned_data.get('password1')
            user = authenticate(request, username=user.username, password=password)
            auth_login(request, user)
            return redirect('home')
    else:
        form = RegistrationForm()
    return render(request, 't5towing/registration.html', {'form': form})

# vehicle search
# this view uses a filter to populate search results based on vin or license
# number and state
def search(request):
    if request.user.is_authenticated():
        vehicle_list = Vehicle.objects.all()
    else:
        if request.GET:
            # by default, a filter will display all results, so we will use some
            # logic to ensure that results only appear when exact matches are made

            # keep track of how many of the fields match the regex
            pattern_tracker = 0

            # regex for at least one letter or number
            pattern = re.compile("^([a-zA-Z0-9]+)$")

            #loop through and increment for any matches
            for key, value in request.GET.items():
                pattern_match = pattern.match(value)
                if pattern_match:
                    pattern_tracker += 1

            # if there are no matches then all fields are empty
            # if we have a match, then we let the filter run on cars that are
            # checked in to the lot
            if pattern_tracker == 0:
                vehicle_list = Vehicle.objects.none()
            else:
                vehicle_list = Vehicle.objects.filter(checkedin=True)
        else:
            vehicle_list = Vehicle.objects.none()

    vehicle_filter = VehicleFilter(request.GET, queryset=vehicle_list)
    return render(request, 'inventory/search.html', {'filter': vehicle_filter })
