# -*- coding: utf-8 -*-
# import our python dependencies
from __future__ import unicode_literals

# import app config
from django.apps import AppConfig

# declare app
class InventoryConfig(AppConfig):
    name = 'inventory'
