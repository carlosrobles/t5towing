# -*- coding: utf-8 -*-
# import our django dependencies
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf.urls import include, url
from django.views.generic.base import TemplateView

# import our views and forms
from inventory import views
from inventory.forms import LoginForm

# configure our app name
app_name = 't5towing'

# set our url patterns
urlpatterns = [
    # /
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),

    # /inventory/
    url(r'^inventory/', include ('inventory.urls'), name='inventory'),

    # /admin/
    url(r'^admin/', admin.site.urls),

    # /register/
    url(r'^register/$', views.registration, name='registration'),

    # /login/
    url(r'^login/', auth_views.login, {'template_name': 't5towing/login.html', 'authentication_form': LoginForm}, name='login'),

    # /logout/
    url(r'^logout/', auth_views.logout, {'template_name': 't5towing/logout.html'}, name='logout'),

    # /metrics
    url(r'^metrics/$', views.metrics, name='metrics'),

    # /paymentsummary/
    url(r'^payment_summary/$', views.payment_summary, name='payment_summary'),

    # /profile/<username>
    url(r'^profile/$', views.profile, name='profile'),
]
