from t5towing.settings.base import *
import dj_database_url

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
PROJECT_DIR = os.path.join(PROJECT_ROOT, '../../inventory')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Database and secret key config for herokuv
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
SECRET_KEY = config('SECRET_KEY')
DATABASES = {
    'default': dj_database_url.config(
        default=config('DATABASE_URL')
    )
}
ALLOWED_HOSTS = ['*']

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, 'static'),
)
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
